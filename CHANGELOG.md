# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.4.0](https://gitlab.com/foundryvtt-pt-br/core/compare/v1.3.0...v1.4.0) (2020-09-03)


### Features

* **pt-br.json:** update translation for latest Foundry v0.7.1 ([2057f53](https://gitlab.com/foundryvtt-pt-br/core/commit/2057f533959391cf1d3d8324c4d4f46bcea3fd03))

## [1.3.0](https://gitlab.com/foundryvtt-pt-br/core/compare/v1.1.0...v1.3.0) (2020-06-08)


### Features

* **pt-br.json:** update translation for latest Foundry v0.6.2 ([d4fdd80](https://gitlab.com/foundryvtt-pt-br/core/commit/d4fdd802474f656ea33821472ac0ee5ea831b200))

## [1.2.0](https://gitlab.com/foundryvtt-pt-br/core/compare/v1.1.0...v1.2.0) (2020-05-24)


### Features

* **pt-br.json:** update translation for latest Foundry v0.6.0 ([1cbe779](https://gitlab.com/foundryvtt-pt-br/core/commit/1cbe77904c5df2066fa15261209b22c6143fbf03))


### [1.1.1](https://gitlab.com/foundryvtt-pt-br/core/compare/v1.1.0...v1.1.1) (2020-05-18)


### Bug Fixes

* **pt-br.json:** shorten some strings to better fit the setup UI ([c954232](https://gitlab.com/foundryvtt-pt-br/core/commit/c954232c8e1d41533c2bc1c05fcb4bc65ed1cf3e))

## [1.1.0](https://gitlab.com/foundryvtt-pt-br/core/compare/v1.0.0...v1.1.0) (2020-05-18)


### Features

* update strings for 0.5.7 update ([1a6b56f](https://gitlab.com/foundryvtt-pt-br/core/commit/1a6b56fce373349202d4644e181279df4212f88f))

## 1.0.0 (2020-04-19)


### Features

* initial release ([74c250e](https://gitlab.com/foundryvtt-pt-br/core/commit/74c250e9ac45fe64608f6dbe53b88d0dd79dd46e))

## Português

Se você quer ajudar com a tradução dos termos basta acessar o nosso projeto no [**Transifex**](https://www.transifex.com/foundryvtt-brasil/fvtt-core/) e começar a traduzir! Caso tenha alguma dúvida você pode nos procurar no Discord através desse [**convite**](https://discord.gg/Muj5AUK).

Caso você queira contrubuir com o repositorio do GitLab de alguma forma, por favor leia as instruções abaixo com atenção.


### Commits

* Esse repositório utiliza a especificação do [*Conventional Commits*](https://www.conventionalcommits.org/pt-br). Se você não está familiarizado com essa convenção não se preocupe, basta rodar o script `npm run commit` ou `yarn commit` caso utilize o yarn, e responder as perguntas para gerar o commit. Commits que estejam fora a convenção não serão aceitos.

* Graças ao [*Conventional Commits*](https://www.conventionalcommits.org/pt-br) o *CHANGELOG* do repositorio pode ser gerado automaticamente através do script `npm run release` ou `yarn release` caso utilize o yarn. Sempre que finalizar o seu commit utilize o script acima para gerar o texto do *CHANGELOG.md* além de atualizar automaticamente a versão do módulo, por favor não atualize a versão manualmente.

### Merge Requests

* Por favor não envie *Merge Requests* diretamente para o branch `master`. Crie um novo branch e submeta uma nova *Merge Request* apartir dele ou do branch `dev`.


___


## English

If you would like to help with the translation of terms,just access our project at [**Transifex**](https://www.transifex.com/foundryvtt-brasil/fvtt-core/) and start translating! If you have any questions you can contact us on Discord through this [**invitation**](https://discord.gg/Muj5AUK).

If you want to contribute to the GitLab repository in any way, please read the instructions below carefully.


### Commits

* This repository uses the [*Conventional Commits*](https://www.conventionalcommits.org) specification . If you are not familiar with this convention don't worry, just run the script `npm run commit` or` yarn commit` if you use yarn and answer the questions to generate the commit. Commits outside the convention will not be accepted.

* Thanks to the [*Conventional Commits*](https://www.conventionalcommits.org) the repository's *CHANGELOG* can be generated automatically through the script `npm run release` or ` yarn release` if you use yarn . Whenever you finish your commit, use the script above to generate the text of *CHANGELOG.md* in addition to automatically updating the module version, please do not update the version manually.

### Merge requests

* Please do not send *Merge Requests* directly to the `master` branch. Create a new branch and submit a new *Merge Request* from it or from the `dev` branch.